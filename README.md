# Project Fizzy Knitting

- Source for [fizzy-knitting.gitlab.io](https://fizzy-knitting.gitlab.io/)

This is the [mkdocs](https://mkdocs.org) static site generator.
Each change to master (even with online editor) will re-generate the site.

## Install & Run Locally

Assume that python3 is installed

```bash
git clone git@gitlab.com:fizzy-knitting/fizzy-knitting.gitlab.io.git
cd fizzy-knitting.gitlab.io
pip3 install requirements.txt
mkdocs serve
```

Point a browser at [http://localhost:25000](http://localhost:25000)

To start on a different port/address use

```bash
mkdocs serve -a localhost:8080
```

