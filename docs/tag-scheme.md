# Fizzy Knitting Tag Scheme

The following is taken from the existing [OSM railway tagging scheme](https://wiki.openstreetmap.org/wiki/OpenRailwayMap/Tagging#Tracks), which is managed by the Open Rail Map community. Some of the items in this list are amendments being proposed by the Fizzy Knitting team. These are marked ($) and are subject to formal OSM change proposals.

# Tags to be Edited - Quick Guide

The following tags should be edited as necessary by Fizzy Knitting Taggers, and are also used to determine how assets are rendered in the map.

### Key: electrified=*
Valid values: no | rail | 4th_rail | contact_line | rail;contact_line | 4th_rail;contact_line

### Key: voltage=[system voltage in Volts]

### Key: frequency=[system frequency in Hertz]

For a more comprehensive view including lines under construction, electrification under construction, proposed new lines etc, read on.

# Tags to be Edited - Full Guide

## Key: electrified=*
**Use:** To tag currently in-service electrification systems with their contact system type.

## Key: construction:electrified=*
**Use:** To tag electrification systems which are in construction, but not yet in service, with their contact system type.

**Status: These tags are essential to correct rendering of the map. Both keys take the values in the table below.**

| **Value** | **Property** | **Description** | **Map Render** |
| --- | --- | --- | --- |
| **yes** | Electrified - system not specified | This tag is to be used when the system of electrification is unknown. **Mappers should use electrified=contact\_line, rail, 4th\_rail, or ground-level\_power\_supply where they are able to identify the system by use of aerial imagery or other means.** | Use to determine colour |
| **no** | Not electrified | Used for non-electrified tracks. | Use to determine colour |
| **contact\_line** | Contact line | Used for tracks electrified with Overhead Contact Line (OCL) / Overhead Contact System (OCS) / Overhead Line Equipment (OLE) | Use to determine colour |
| **rail** | 3rd contact rail | Used for tracks electrified with a 3rd rail system, with one additional rail on insulators, using shoe pickup by the train, and traction return via the running rail(s) | Use to determine colour |
| **4th\_rail** ($) | 4th contact rail | Used for tracks electrified with a 4th rail system, with two additional rails on insulators and two shoe pickup by the train, and traction current returning via one of the insulated rails | Use to determine colour |
| **contact\_line;rail** ($) | Dual electrified - overhead contact line and 3rd rail | Used for a single track electrified with both Overhead Contact Line (OCL) and 3rd rail. | Use to determine colour |
| **contact\_line;4th\_rail** ($) | Dual electrified - overhead contact line and 4th rail | Used for a single track electrified with both Overhead Contact Line (OCL) and 4th rail. | Use to determine colour |
| **ground-level\_power\_supply** | ground-level power supply | Used for tracks electrified with [ground-level power supply](http://en.wikipedia.org/wiki/en:ground-level_power_supply) on Wikipedia. | None in UK |

## Key: voltage=*
**Use:** To tag currently in-service electrification systems with their contact system voltage.

## Key: construction:voltage=*
**Use:** To tag electrification systems which are in construction, but not yet in service, with their contact system voltage.

**Status: These tags are essential to correct rendering of the map. Both keys take the values in the table below.**

| **Value** | **Property** | **Description** | **Map Render** |
| --- | --- | --- | --- |
| **voltage in Volts** | Voltage | Voltage of the railway electrification system. [Current Systems](http://en.wikipedia.org/wiki/en:List_of_current_systems_for_electric_rail_traction) on Wikipedia. **For dual electrified areas, use overhead contact line voltage, followed by 3rd or 4th rail voltage. For instance, a track electrified with both 25000V 50Hz overhead contact line and 750V DC 3rd rail would be tagged voltage=25000;750** ($) | Use to determine colour |

## Key: frequency=*
**Use:** To tag currently in-service electrification systems with their electrical frequency.

## Key: construction:frequency=*
**Use:** To tag electrification systems which are in construction, but not yet in service, with their electrical frequency.

**Status: These tags are not essential to correct rendering of the map, but are important to fill out correctly as they will appear in the supplementary information pop-up for each track.**

| **Value** | **Property** | **Description** | **Map Render** |
| --- | --- | --- | --- |
| **frequency in Hertz** | Frequency | Frequency of the railway electrification system. Use 0 for direct current. [More information](https://wiki.openstreetmap.org/wiki/Key:frequency). **For dual electrified areas, use overhead contact line voltage followed by 3rd or 4th rail voltage. For instance, a track electrified with both 25000V 50Hz overhead contact line and 750V DC 3rd rail would be tagged frequency =50;0** ($) | Show in text box |

## Key: electrified:rail=*
This tag can be edited globally once the correct electrified=* value is set. Taggers do not need to edit this value for individual ways.

| **Value** | **Property** | **Description** | **Map Render** |
| --- | --- | --- | --- |
| **top/side/bottom** | Contact Type | The contact type of the third rail. | Show in text box |

# Tags to be used in Render

The following tags should not generally be edited by Fizzy Knitting Taggers. They are to be used to determine which assets are rendered, and the how they should be rendered, and which tags should be ignored when rendering.

## Key: railway=*

| **Value** | **Property** | **Description** | **Map Render** |
| --- | --- | --- | --- |
| **rail** | (active) track | A track that is used on a regular basis. | Use to determine line style &amp; thickness |
| **proposed** | Planned track | A planned track, still in the design phase (no construction yet). | Use to determine line style |
| **construction** | Track under construction | [construction](https://wiki.openstreetmap.org/wiki/Key:construction)=\* can be used for a more precise description. It obtains the value usually given to [railway](https://wiki.openstreetmap.org/wiki/Key:railway)=\*, such as [railway](https://wiki.openstreetmap.org/wiki/Key:railway)=[construction](https://wiki.openstreetmap.org/wiki/Tag:railway%3Dconstruction) and [construction](https://wiki.openstreetmap.org/wiki/Key:construction)=[narrow\_gauge](https://wiki.openstreetmap.org/wiki/Tag:construction%3Dnarrow_gauge). | Use to determine line style |
| **_disused_** | _Disused track_ | _The track is preserved (can still be seen) but is no longer in use and possibly overgrown._ | _Do not render_ |
| **_abandoned_** | _Abandoned track_ | _While the track no longer holds any rails or signals, the former line (or even a trackbed) can still be seen. These remains might include embankments, trenches, bridges and tunnels._ | _Do not render_ |
| **_razed_** | _Overbuilt track_ | _A former track that has been built upon. While some remains might still be seen, the former route is subject to educated guesses for the most part._ | _Do not render_ |
| **narrow\_gauge** | Narrow-gauge track | A track with a gauge (width) narrower than typical to this country. A gauge might be defined by the tag [gauge](https://wiki.openstreetmap.org/wiki/Key:gauge)=\* using the unit of millimeters. For tracks with three rails, the tag [railway](https://wiki.openstreetmap.org/wiki/Key:railway)=[rail](https://wiki.openstreetmap.org/wiki/Tag:railway%3Drail) can be used, specifying both gauges by [gauge](https://wiki.openstreetmap.org/wiki/Key:gauge)=\*, separated by a semicolon (such as 1435;1000). | Show? |
| **light\_rail** | City railway, sometimes &quot;Suburban&quot; | City railway and tramlike underground trains, sometimes &quot;suburban.&quot; These can be heavy trains, differ in power system (usually electric, occasionally diesel), have their own signals and are a distinct vehicle fleet. These usually are in a dedicated right-of-way, rarely having level crossings with road traffic. However, in denser urban areas these can be street-running like a tram, even while remaining tagged [railway](https://wiki.openstreetmap.org/wiki/Key:railway)=[light\_rail](https://wiki.openstreetmap.org/wiki/Tag:railway%3Dlight_rail). North American examples include San Diego Trolley, San Francisco Muni, Portland MAX, Calgary CTrain, Dallas DART, Cleveland Blue and Green Lines, The Tide in Norfolk, Baltimore Light Rail and Buffalo Metro Rail where [passenger](https://wiki.openstreetmap.org/wiki/Key:passenger)=[urban](https://wiki.openstreetmap.org/wiki/Tag:passenger%3Durban) might be found more frequently (instead of [passenger](https://wiki.openstreetmap.org/wiki/Key:passenger)=[suburban](https://wiki.openstreetmap.org/wiki/Tag:passenger%3Dsuburban), which are considered more &quot;commuter&quot; railways using full-sized &quot;heavy rail&quot; railcars). | Use to determine line style &amp; thickness |
| **subway** | Subway/Metro | Underground railway in larger cities, powered mostly by third rail. It has its own vehicles and signal system. Sometimes it also comes to the surface or is on an aerial causeway for a segment. Do not map ordinary railway, which goes partially underground, with this tag! (Instead, use [layer](https://wiki.openstreetmap.org/wiki/Key:layer)=[-1](https://wiki.openstreetmap.org/wiki/Tag:layer%3D-1) or a &quot;deeper&quot; negative value and [tunnel](https://wiki.openstreetmap.org/wiki/Key:tunnel)=[yes](https://wiki.openstreetmap.org/wiki/Tag:tunnel%3Dyes)). Subway/Metro is usually considered &quot;heavy rail&quot; and is always distinct from &quot;light rail.&quot; | Use to determine line style &amp; thickness |
| **tram** | Tram | Mostly overground on-street laid tracks (&quot;street-running&quot;). It is common that vehicles like motorcars can share traffic lanes with trams. Some special railways similar to trams might be also mapped using this tag. Larger parts of tram tracks can also go underground (use [layer](https://wiki.openstreetmap.org/wiki/Key:layer)=[-1](https://wiki.openstreetmap.org/wiki/Tag:layer%3D-1) or a &quot;deeper&quot; negative value and/or [tunnel](https://wiki.openstreetmap.org/wiki/Key:tunnel)=[yes](https://wiki.openstreetmap.org/wiki/Tag:tunnel%3Dyes)). Some tram routes also use ordinary railway tracks. These should me mapped as ordinary railway tracks ([railway](https://wiki.openstreetmap.org/wiki/Key:railway)=[rail](https://wiki.openstreetmap.org/wiki/Tag:railway%3Drail)). | Use to determine line style &amp; thickness |
| **_miniature_** | _Miniature railway_ | _Small railways in parks for entertainment or as a tourist attraction, mostly narrow gauge (up to 600mm). These are often a &quot;scale&quot; of a &quot;standard&quot; railroad size, for example &quot;1/4 scale miniature railroad.&quot;_ | _Do not render_ |

## Key: abandoned:electrified=*

| **Value** | **Property** | **Description** | **Map Render** |
| --- | --- | --- | --- |
| **_yes_** | _De-electrified_ | _Used for tracks that were formerly electrified, but have since been removed._ | _Do not render_ |
| **_contact\_line_** | _Contact line_ | _Used for tracks with a catenary/contact line above them._ | _Do not render_ |
| **_rail_** | _Contact rail_ | _Used for tracks electrified by a contact rail/third rail._ | _Do not render_ |

## Key: deelectrified=*

| **Value** | **Property** | **Description** | **Map Render** |
| --- | --- | --- | --- |
| **_same values as electrified_** | _De-electrified_ | _Used for tracks that were formerly electrified, but have since been removed.  **The usage of this tag is discouraged. Use abandoned:electrified instead, with optional usage of abandoned:frequency and abandoned:voltage.**_ | _Do not render_ |