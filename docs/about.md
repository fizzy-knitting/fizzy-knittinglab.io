About Fizzy Knitting
====================

## Overview + Mission

This is a quick summary of the 'mad idea' from the [Scope](/scope) is to:

- show a map of UK railway network, differentiating lines which are electrified
- using ORM = [Open Railway Map](https://www.openrailwaymap.org/)
- which is a subset of OSM = [Open Street Map](https://www.openstreetmap.org/)
- and modify it 'live' so it's always up to date
- and a lot of editing tags already done and debate.....
- presented with [colours](colours.md) that might not apply else where internationally

Identified issues are:

- whether rails are electric or not
- and also if
- OLE (overhead high.25kV.AC)
- a third rail common in SE = DC.600
- a fourth rail only London.Underground.Wombling and return rail
- or indeed all the above together.. nightmare right? impossible ?? ;-)))
- tagging in OSM
- and gaining consensus in ORM and OSM
- Its already moving fast upstream see [Links](links.md)

## Project Details

The project is

- open source is first principle as using open source everywhere and therby part of eco.system
- Debate was moved from twitter to [chat at gitter/fizzy-knitting](https://gitter.im/fizzy-knitting/community)
- Code and www is hosted as a group at [gitlab.com/fizzy-knitting](https://gitlab.com/fizzy-knitting)
- Using gitlab hosting and CI = www site (and its FOSS.. thanks gitlab)

## History

Once upon a time on a hot day in May 2020 amidst a pandemic.lockdown, a rail engineer tweeted....

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">If anyone out there has experience of building an instance of <a href="https://twitter.com/openstreetmap?ref_src=twsrc%5Etfw">@openstreetmap</a>, could you get in touch? I&#39;ve had a dangerous idea, but I don&#39;t have all the necessary technical skills to implement</p>&mdash; Garry Keenor (@25kV) <a href="https://twitter.com/25kV/status/1264854274506395648?ref_src=twsrc%5Etfw">May 25, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">I basically want <a href="https://t.co/orAVg5k9ol">https://t.co/orAVg5k9ol</a> but UK only, and using the electrification tags already in the OSM to generate an electrification map</p>&mdash; Garry Keenor (@25kV) <a href="https://twitter.com/25kV/status/1264914154864992258?ref_src=twsrc%5Etfw">May 25, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Hence the project `Fizzy Knitting` was born, a pandemic baby!
