# Overpass Query

Use this query in the [Overpass API](http://overpass-turbo.eu/) to generate spot-maps for checking tagging, colour, symbology etc.

## Query:

```raw
[out:json];

// gather results

(

  way["railway"]({{bbox}});

  // apply styles for each rail electrification type

 

  {{style:

 

  /* apply widths and dashes for railway types */

  /* default colour is yellow before overlaying other colors for electrification types */

 

  /* standard railway */

  way[railway=rail]

{color:yellow; width:2; opacity:1;dashes:1;}

 

  /* light and other railways */

  way[railway=subway]

{color:yellow; width:2; opacity:1;dashes:1;}

  way[railway=tram]

{color:blue; width:2; opacity:1;dashes:1;}

  way[railway=light_rail]

{color:yellow; width:1; opacity:1;dashes:1;}

  way[railway=narrow_gauge]

{color:yellow; width:1; opacity:1;dashes:1;}

  way[railway=miniature]

{color:yellow; color:black; width:1; opacity:1;dashes:1;}

  way[railway=monorail]

{color:yellow; width:1; opacity:1;dashes:1;}

  way[railway=funicular]

{color:yellow; width:1; opacity:1;dashes:1;}

  way[railway=preserved]

{color:yellow; width:1; opacity:1;dashes:1;}

 

  /* former railways */

  way[railway=abandoned]

{color:brown; width:2; opacity:0;dashes:5,5;}

  way[railway=razed]

{color:brown; width:2; opacity:0;dashes:5,5;}

  way[railway=disused]

{color:brown; width:2; opacity:0;dashes:5,5;}

  way[railway=historical]

{color:brown; width:2; opacity:0;dashes:5,5;}

  way[railway=historic]

{color:brown; width:2; opacity:0;dashes:5,5;}

  way[railway=dismantled]

{color:brown; width:2; opacity:0;dashes:5,5;}

 

  /* future railways */

  way[railway=proposed]

{color:black; width:2; opacity:1;dashes:10,10,2,10}

  way[railway=construction]

{color:black; width:2; opacity:1;dashes:5,5;}


  
  /* tunnels */

   way[tunnel][tunnel!=no]
{ opacity:1; }
 

  /* now apply colors for electrification */

 

  /* none */

  way[electrified=no]

{color:#2c2e35;}

 

  /* 3rd rail at each voltage */

  way[electrified=rail][voltage=550]

{color:#b3e297;}

  way[electrified=rail][voltage=600]

{color:#9cd977;}

  way[electrified=rail][voltage=630]

{color:#7bc64e;}
  
  way[electrified=rail][voltage=660]

{color:#7bc64e;}

  way[electrified=rail][voltage=750]

{color:#09843b;}

 

  /* 4th rail at each voltage */

  way[electrified=4th_rail][voltage=630]

{color:#0a3797;}

  way[electrified=4th_rail][voltage=660]

{color:#11a7e0;}

  way[electrified=4th_rail][voltage=750]

{color:#11a7e0;}

 

  /* OHL at each voltage */
  
  way[electrified=contact_line][voltage=0]

{color:#9ca3a9;}

  way[electrified=contact_line][voltage<=550]

{color:#e69c65;}

  way[electrified=contact_line][voltage=600]

{color:#bd5d16;}

  way[electrified=contact_line][voltage=750]

{color:#ffce36;}

  way[electrified=contact_line][voltage=1500]

{color:#ff893f;}

  way[electrified=contact_line][voltage=25000]

{color:#ff3818;}

 

  /* multiple electrification types */

  way[electrified=contact_line;rail][voltage=25000;750]

{color:#fe85a8;}
  
  way[electrified=contact_line;rail][voltage=25000;660]

{color:#fe85a8;}

  way[electrified=contact_line;4th_rail][voltage=25000;630]

{color:#d6296e;}
  
  way[electrified=contact_line;4th_rail][voltage=25000;660]

{color:#d6296e;}

  way[electrified=contact_line;4th_rail][voltage=25000;750]

{color:#dd1762;}
  
  
  
  /* future electrification */ 
  

  	       way[construction:electrified=contact_line][construction:voltage=750]
{color:#ffce36;}  
  way[construction:electrified=contact_line][construction:voltage=25000]
{color:#ff3818;}
  
}}

);

// print results

out body;

>;

out skel qt;

```
