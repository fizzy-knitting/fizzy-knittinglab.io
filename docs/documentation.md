# Fizzy Knitting Documentation

This is the landing page for all of our documentation.

## Project Scope
Read about the [scope and aims](scope.md) of the project.

## OSM Tagging Scheme

Read [tagging scheme](tag-scheme.md) to learn about which tags we use and what values to use, including changes which we are proposing to the OSM project.

## Colour Scheme

Read [colour scheme](colours.md) to see our colour coding and map symbology.

## Taggers Guide
Looking to edit the map and improve it? Read our [taggers guide](taggers-guide.md).