

var topLeft = L.latLng(58.0, -7.0);
var bottomRight = L.latLng(50.0, 2.22);

var UKBounds = L.latLngBounds(topLeft, bottomRight);

var radius = 3;
var weight = 1;
var linewidth = 2;
var log2 = Math.log(2.0);
var minZoom = 3;
var maxZoom = 18;
//var map = L.map('map').setView([54.533, -2.53], 6);
//var voltages = ["25000", "1500", "750", "630", "600", "550"]
var voltages = ["660", "750", "1000", "1500", "3000", "6000", "11000", "15000", "20000", "25000", "50000"];
var colours = ["#4300BD","#5A00B2","#6F00A7","#86009C","#FFCC00","#FFAF00","#FF9400","#FF7D00","#FF6600","#FF4500","#F00000","#D00000"];

var baseControl;

function get_colour(p) {
     var e = p.electrified;
     var f = p.frequency;
     var v = p.voltage;
     var r = p.railway
     var fail = "#ff00ff";
     if (v && Number(v) < 550) v = "550";
     if (e == "rail" || e == "3rd_rail") {
         var colours = { "550":   "#b3e297",
                         "630":   "#7bc64e",
                         "750":   "#09843b",
                         "1000":  "#025323",};
         if (v in colours) return colours[v];
         return fail;
     }
     if (e == "4th_rail") {
         var colours = { "630":   "#0a3797",
                         "750":   "#35aad3",};
         if (v in colours) return colours[v];
         return fail;
     }
     if (e == "contact_line" || e == "yes") {
         var colours = { "550":   "#e69c65",
                         "600":   "#bd5d16",
                         "750":   "#ffce36",
                         "1500":  "#ff893f",
                         "25000": "#ff3818",
                         "50000": "#d00000",};
         if (v in colours) return colours[v];
         return fail;
     }
     if (e == "contact_line;rail") {
         return "#fe85a8";
     }
     if (r && (e == "no" || ! e))
         return "#000000";
     if (v && Number(v) > 25000) return fail; //"#d00000";
     if (v == "0") return "#9ca3a9";
     return fail;
 }

function onEachFeature(feature, layer) {
     var this_feature = feature.properties;
     var popupContent;
     if (this_feature.name)
         popupContent = this_feature.name;
     else
         popupContent = this_feature.railway;
     var lookup = {
         "osm_id": "OSM ID",
         "place": "Place",
         "natural": "Natural",
         "place": "Place",
         "highway": "Road",
         "ref": "CRS",
         "admin_level": "admin",
         "boundary": "boundary",
         "voltage": "voltage",
         "frequency": "frequency",
         "electrified": "electrified"
     }
     var k = Object.keys(this_feature).filter(i=>(i != "type" && i != "geometry" && i != "name" && i != "is_in" && i != "z_order" && i != "railway"))
         for (var i = 0; i < k.length; i++) {
             if (k[i] in this_feature) {
                 popupContent += '<br>' + lookup[k[i]] + ': ' + this_feature[k[i]];
             }
         }
     layer.bindPopup(popupContent);
 }

function CreateMap(){


    var map = L.map('map_div', {
        center: UKBounds.getCenter(),
        maxBounds: UKBounds,
        maxBoundsViscosity: 1.0

    }).setView(UKBounds.getCenter(), 7);
    map.setMaxBounds(UKBounds);

    //== Base maps

    var CartoDB_Positron = L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
        subdomains: 'abcd',
        //maxZoom: 19
    }).addTo(map);

    var OSMLayer = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        //maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoicGVkcm9tb3JnYW4iLCJhIjoiY2s3Nzg4azF3MDRwdzNvcGh5cXE5bjAyZiJ9.Bry79aB3OoladY6R94rxYQ'
    });

    var Esri_WorldImagery = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
        attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
    });

    var Esri_WorldShadedRelief = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Shaded_Relief/MapServer/tile/{z}/{y}/{x}', {
        attribution: 'Tiles &copy; Esri &mdash; Source: Esri',
        maxZoom: 13
    });

//    var ORMLayer = L.tileLayer('https://{s}.tiles.openrailwaymap.org/standard/{z}/{x}/{y}.png', {
//        //maxZoom: 19,
//        attribution: 'Map data: &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors | Map style: &copy; <a href="https://www.OpenRailwayMap.org">OpenRailwayMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)'
//    });

    var baseMaps = {
        "CartoDB_Positron": CartoDB_Positron,
        "OSM": OSMLayer,
        "Esri_WorldImagery": Esri_WorldImagery,
        "Esri_WorldShadedRelief": Esri_WorldShadedRelief
    }

    //==== Overlays

    var ORMLayer = L.tileLayer('https://{s}.tiles.openrailwaymap.org/standard/{z}/{x}/{y}.png', {
        //maxZoom: 19,
        attribution: 'Map data: &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors | Map style: &copy; <a href="https://www.OpenRailwayMap.org">OpenRailwayMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)'
    });

//    var RailwayHistoryLayer = L.tileLayer('https://www.railwayhistory.org/electric/tiles/{z}/{x}/{y}.png', {
//        attribution: 'Map data: &copy; <a href="https://www.railwayhistory.org">railwayhistory.org</a> contributors )'
//    }).addTo(map);


         d3.json("https://fizzy-knitting.gitlab.io/latest/output-all.json").then(function(d) {

             willLayer = L.geoJSON(d, {
                 style: function(feature) {
                     switch (feature.geometry.type) {
                         case 'Point':
                             return {
                                 color: get_colour(feature.properties),
                                 radius: radius,
                                 weight: weight
                             };
                         case 'LineString':
                             return {
                                 color: get_colour(feature.properties),
                                 weight: linewidth
                             };
                         case 'MultiLineString':
                             return {
                                 color: get_colour(feature.properties),
                                 weight: linewidth
                             };
                         case 'MultiPolygon':
                             return {
                                 weight: linewidth,
                                 opacity: 1.0,
                                 color: "Green",
                                 fillColor: "Green",
                                 fillOpacity: 0.2
                             };
                         case 'GeometryCollection':
                             return {
	                             radius: radius,
                                 weight: linewidth,
                                 opacity: 1.0,
                                 color: "Green",
                                 fillColor: "GreenYellow",
                                 fillOpacity: 0.2
                             };

                         default:
                             return {
                                 weight: weight
                             };
                     }
                 },
                 onEachFeature: onEachFeature,
                 pointToLayer: function(feature, latlng) {
                     return L.circleMarker(latlng, {
                         opacity: 1,
                         fillOpacity: 0.8
                     });
                 }
             }).addTo(map);
             baseControl.addOverlay(willLayer, "Latest Fizzy");
         });




    var overlayMaps = {
        "OpenRailwayMap": ORMLayer,
        //"Railway History": RailwayHistoryLayer
        //"Latest Fizzy": willLayer
    }
    baseControl = L.control.layers(baseMaps, overlayMaps);
    baseControl.addTo(map)

    return map;
}



function SetupMegaMap(){


    var map = CreateMap()




}




