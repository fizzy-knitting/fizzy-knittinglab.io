function map_move_end(evt) {
    var view = evt.map.getView();
    var zoom = view.getZoom()
    var center = ol.proj.transform(
      view.getCenter(), "EPSG:3857", "EPSG:4326"
    );
    var x = (Math.round(center[1] * 10000) / 10000);
    var y = (Math.round(center[0] * 10000) / 10000)
    var zoom = (Math.round(zoom * 10) / 10)
    var map_param = "map=" + zoom + '!' + x + '!' + y;

    var h = window.location.hash || '#';
    if (h.indexOf('?') < 0)
        h = h + '?' + map_param;
    else if (h.indexOf('map=') >= 0)
        h = h.replace(new RegExp("map=[^&]*"), map_param);
    else
        h = h + '&' + map_param;

    window.history.replaceState(window.history.state, document.title, h);
}

function create_style() {
    var colors = {
      ocl550: '#e69c65',
      ocl600: '#bd5d16',
      ocl750: '#ffce36',
      ocl1500: '#ff893f',
      ocl25000: '#ff3818',
      ocl0: '#9ca3a9',

      r3_550: '#b3e297',
      r3_630: '#7bc643',
      r3_750: '#09843b',

      r4_630: '#0a3797',
      r4_750: '#11a7e0',

      ocl25000_r3_750: '#fe85a8',
      ocl25000_r4_750: '#9d176s',

      carbon: '#2c2e35',
      other: '#ff00ff',
    };

    var widths = {
      rail: 1.5,
      tran: 0.75,
    }

    var stroke = new ol.style.Stroke({color: '', width: 1.2});
    var line = new ol.style.Style({stroke: stroke});
    var stroke2 = new ol.style.Stroke({color: '', width: 1.2});
    var line2 = new ol.style.Style({stroke: stroke2});
    var styles = [];

    return function(feature, resolution) {
          var length = 0;
          var railway = feature.get('railway');
          var electrified = feature.get('electrified');
          var voltage = feature.get('voltage');
          var elcon = feature.get('construction:electrified');

          if (typeof elcon != "undefined") {
            //console.log(elcon);

            if (railway == 'rail' || railway == 'subway') {
              stroke2.setWidth(widths.rail);
              stroke2.setLineDash(null);
              stroke2.setColor(colors.carbon);
              styles[length ++] = line2;
              railway = "construction";
            }
            else if (railway == 'light_rail' || railway == 'tram') {
              stroke2.setWidth(widths.tram);
              stroke2.setLineDash(null);
              stroke2.setColor(colors.carbon);
              styles[length ++] = line2;
              railway = "construction";
            }

            if (railway = "construction") {
              electrified = feature.get('construction:electrified');
              voltage = feature.get('construction:voltage');
            }
          }

          if (railway == 'rail' || railway == "subway") {
            stroke.setWidth(widths.rail);
            stroke.setLineDash(null);
          }
          else if (railway == 'light_rail' || railway == 'tram') {
            stroke.setWidth(widths.tram);
            stroke.setLineDash(null);
          }
          else if (railway == 'construction') {
            stroke.setWidth(widths.rail);
            stroke.setLineDash([10,10]);
          }
          else {
            return undefined;
          }
          stroke.setColor(colors.other);
          if (electrified == 'contact_line') {
            if (voltage == '25000') {
              stroke.setColor(colors.ocl25000);
            }
            else if (voltage == '1500') {
              stroke.setColor(colors.ocl1500);
            }
            else if (voltage == '750') {
              stroke.setColor(colors.ocl750);
            }
            else if (voltage == '600') {
              stroke.setColor(colors.ocl600);
            }
            else if (voltage == '0') {
              stroke.setColor(colors.ocl0);
            }
            else if (Number(voltage) <= 550) {
              stroke.setColor(colors.ocl550);
            }
          }
          else if (electrified == 'rail') {
            if (voltage == '750') {
              stroke.setColor(colors.r3_750);
            }
            else if (voltage == '630' || voltage == '660') {
              stroke.setColor(colors.r3_630);
            }
            else if (Number(voltage) <= 550) {
              stroke.setColor(colors.r3_550);
            }
          }
          else if (electrified == '4th_rail') {
            if (voltage == '750') {
              stroke.setColor(colors.r4_750);
            }
            else if (voltage == '630' || voltage == '660') {
              stroke.setColor(colors.r4_630);
            }
          }
          else if (electrified == 'contact_line;rail') {
            if (voltage == '25000;750') {
              stroke.setColor(colors.ocl25000_r3_750);
            }
          }
          else if (electrified == 'rail;contact_line') {
            if (voltage == '750;25000') {
              stroke.setColor(colors.ocl25000_r3_750);
            }
          }
          else if (electrified == 'contact_line;4th_rail') {
            if (voltage == '25000;750') {
              stroke.setColor(colors.ocl25000_r4_750);
            }
          }
          else if (electrified == '4th_rail;contact_line') {
            if (voltage == '750;25000') {
              stroke.setColor(colors.ocl25000_r4_750);
            }
          }
          else if (electrified == 'no' || typeof electrified == "undefined") {
            stroke.setColor(colors.carbon);
          }
          else {
            stroke.setColor('#ff00ff');
          }
          styles[length ++] = line;
          styles.length = length;

          return styles;
        }
}

function setup_openlayers_map(){

      var init_view = { center: [-1.538, 54.0728], zoom: 7 };
      var url_view = decodeURI(window.location.hash.replace(
               new RegExp("^(?:.*[&\\?]map(?:\\=([^&]*))?)?.*$", "i"), "$1"));
      if (url_view) {
        var parts = url_view.split('!');
        if (parts.length === 3) {
          init_view = { zoom : parseInt(parts[0], 10),
                        center : [parseFloat(parts[2]), parseFloat(parts[1])] };
        }
      }
      if (init_view.center[0] < -180 || init_view.center[0] > 180)
        init_view.center[0] = init_view.center[0] % 180;
      if (init_view.center[1] < -90 || init_view.center[1] > 90)
        init_view.center[1] = init_view.center[1] % 90;


      var layerOSM = new ol.layer.Tile({
                source: new ol.source.OSM({
            }),
            title: "Open Street Map",
            opacity: 0.3,
            type: 'base',
      });

      var layerBing = new ol.layer.Tile({
            visible: true,
            preload: Infinity,
            type: 'base',
            title: "Bing Aerial",
            source: new ol.source.BingMaps({
              key: 'AiPqa5j8TFKPWymGQoTODNWcfQPC2W102QFskQ4DSJQTwksI9jNmmjVVWSuix-Vu',
              imagerySet: "Aerial"
              // use maxZoom 19 to see stretched tiles instead of the BingMaps
              // "no photos at this zoom level" tiles
              // maxZoom: 19
            })
          });


      var layerEsri = new ol.layer.Tile({
            source: new ol.source.XYZ({
                attributions: ['Powered by Esri',
                               'Source: Esri, DigitalGlobe, GeoEye, Earthstar Geographics, CNES/Airbus DS, USDA, USGS, AeroGRID, IGN, and the GIS User Community'],
                attributionsCollapsible: false,
                url: 'https://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
                maxZoom: 23
              }),
              title: "Esri Satellite",
              visible: false,
              type: 'base',
              //opacity: 0.8,
        });

       var layerFizzyMapnick = new ol.layer.VectorTile({
        declutter: true,
        source: new ol.source.VectorTile({
          format: new ol.format.MVT(),
          url: "https://www.railwayhistory.org/electric/tiles/vector/{z}/{x}/{y}",
        }),
        style: create_style(),
        title: "Fizzy - Map Tiles"
      });


      var layerFizzyVector = new ol.layer.Vector({
          source: new ol.source.Vector({
            url: 'https://fizzy-knitting.gitlab.io/latest/output-all.json',
            format: new ol.format.GeoJSON()
          }),
//           style: function(feature) {
//            style.getText().setText(feature.get('name'));
//            return style;
//          },
          visible: false,

          title: "Fizzy - Vector.geoJson (slow load)"
        });

      var map = new ol.Map({
        target: 'map',
        controls: ol.control.defaults().extend([
            new ol.control.ScaleLine(),
            new ol.control.FullScreen()
        ]),
        layers: [
            new ol.layer.Group({
                title: "Base Maps",
                //type: 'base',
                combine: false,
                layers: [
                    layerBing,
                    layerEsri,
                    layerOSM
                ]
            }),

             new ol.layer.Group({
                title: "Overlays",
                fold: "open",
                layers: [
                    layerFizzyVector,
                    layerFizzyMapnick
                ]
             }),

        ],
        view: new ol.View({
          center: ol.proj.transform(init_view.center, "EPSG:4326", "EPSG:3857"),
          zoom: init_view.zoom,
          maxZoom: 18,
          constrainResolution: true
        }),
      });
      map.on('moveend', map_move_end);

    var layerSwitcher = new ol.control.LayerSwitcher({
        tipLabel: 'Légende', // Optional label for button
        groupSelectStyle: 'children' // Can be 'children' [default], 'group' or 'none'
    });
    map.addControl(layerSwitcher);

}