Colours
=======

This is the latest 'proposed' color schema for the map:

- The palette is not official to upstream to OSM and ORM.. see [scope](/scope/)
- For feedback, [raise an issue](https://gitlab.com/fizzy-knitting/fizzy-knitting.gitlab.io/-/issues/)

## Overview

* Black: non-electrified
* Orange, red, peach spectrum: overhead contact line (LV and HV, AC or DC)
* Green spectrum: 3rd rail DC (South East UK and Merseyside)
* Blue spectrum: 4th rail DC (London Underground)
* Purple-violet spectrum: dual electrified tracks

![Screenshot](/images/colourcoding.png)
