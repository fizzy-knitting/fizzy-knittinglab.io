Scope
==============

## Purpose of the Project

To develop and publish an online slippy map of the UK rail network which provides, by 
appropriate use of symbology, information on the UK’s electrified and non-electrified network.

## Must Have

- Freely available online
- Minimal mapping effort, re-use of existing OSM mapping data and pre-existing software stack wherever possible
- Updates based on OSM diffs
- Foregrounds the whole rail network, mainline railways, trams, metros, high speed lines, backgrounds other data
- Station labels to provide context
- Differentiates:
    - Overhead line
    - 3rd rail top contact
    - 4th rail top contact
    - 3rd rail bottom contact (Docklands Light Railway)
    - 3rd rail / OLE dual voltage
    - 4th rail / OLE dual voltage
    - Mainline / tram / metro / high speed
    - Surface / tunnel running
    - Existing / railway under construction / electrification under construction
- Has a key
- Looks great – good cartography!

## Should Have
- Ability to switch background mapping e.g. general OSM view or blank background
- Shows voltage
- Shows AC or DC (use frequency tag)
- Ability to share a link to a particular map view
- Ability to switch mainline/tram/metro/underground systems on and off

## Could Have
- Additional rail-specific data e.g. Engineer’s Line References. Need to discuss as will lead to scope creep otherwise
- OLE equipment type (e.g. Mark 1) shown as text
- Electrification completion dates as tags and a “year” slider to show development of the UK network (this could be tricky in practice but I’d like to explore it as phase 2)

## Won’t Have
- Lots of bells and whistles – keep it simple!

