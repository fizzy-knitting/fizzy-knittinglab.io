<img style="float:right;" src="images/electrical_hazard.jpg"/>

## WARNING: ALL RAILWAYS AND ELECTRIFICATION SYSTEMS DESCRIBED IN THIS MAP ARE LIVE AND DANGEROUS AT ALL TIMES!

**Stay off the tracks. Stay away from electrified lines.** The power running through electrification systems is 100 times stronger than your supply at home. Electricity is easily the most dangerous factor in stepping on the track – it’s always switched on and nine out of ten people die when they’re struck by it.

Others are left with life-changing injuries, including burns and amputations; and that’s not to mention the emotional scars that will be left behind. Electricity is easily underestimated. People don’t realise it can jump – so you don’t even need to touch a cable to be seriously injured.

To find out more visit the [You vs. Train website](http://www.youvstrain.co.uk/) or [watch this hard hitting video](http://www.youvstrain.co.uk/film3) on the risks of electric shock (trigger warning: contains descriptions of electric shock injuries).

## Health Warnings on Our Map

**TL;DR: Our map is likely to contain errors and MUST NOT be used for safety-critical work.**  

The intent of this map is to provide a useful overview of railway electrification in the UK. It is generated using data from the [Open Street Map project](https://www.openstreetmap.org/) - a map that is editable by anyone. **As such, we cannot warrant any information held in our map - it is subject to change from sources outside of our control at any time**.

We endeavour to keep the network and electrification data as correct, complete and up to date as possible. The electrification data at route level will generally be reliable; but **electrification data at track and location level is not 100% correct and should not be relied upon**.

The information in this map is distributed on an "As Is" basis, without warranty. While every precaution has been taken in the preparation of the map, ElectricRailMap.co.uk shall not have any liability to any person or entity with respect to any loss or damage caused or alleged to be caused directly or indirectly by the information contained in the map.

