External & Upstream Links
=========================

## Open Railway Map (ORM)

- Website: [openrailwaymap.org](https://www.openrailwaymap.org)
- Electrification map prototype: [map](https://tiles.openrailwaymap.org/#osm.org/10/53.500000/9.950000&overlays=electrification)
- Repos: [github.com/OpenRailwayMap/OpenRailwayMap](https://github.com/OpenRailwayMap/OpenRailwayMap)
- Mailing list: [lists.openrailwaymap.org](https://lists.openrailwaymap.org/mailman3/hyperkitty/list/openrailwaymap@openrailwaymap.org/)
- Current ORM tagging scheme: [ORM tagging scheme](https://wiki.openstreetmap.org/wiki/OpenRailwayMap/Tagging#Tracks)
- OPur discussion with the ORM group: [lists.openrailwaymap.org](https://lists.openrailwaymap.org/mailman3/hyperkitty/list/openrailwaymap@openrailwaymap.org/thread/U7CBSAH6EZBPJPNOEXF2COVZUAO7UTYS/)

## Open Street Map (OSM)

- Current OSM electrification tagging scheme: [OSM tagging scheme](https://wiki.openstreetmap.org/wiki/Key:electrified)
- Our discussion with OSM community: [wiki.openstreetmap.org](https://wiki.openstreetmap.org/wiki/User_talk:GazzerK#3rd_rail_and_4th_rail_values_of_electrified)
- OSM change proposal process: [Process](https://wiki.openstreetmap.org/wiki/Proposal_process#Proposal_Page_Template)
- Our change proposal for 4th rail: [Proposal](https://wiki.openstreetmap.org/wiki/Proposed_features/3rd_and_4th_rail)
- Our change proposal for dual electrified areas: [Proposal](https://wiki.openstreetmap.org/wiki/Proposed_features/dual_voltage_electrification)
- OSM change discussions: [mailing list](https://lists.openstreetmap.org/pipermail/tagging/)

## Analysis and Queries

- [Tag analysis and conversion](https://www.dropbox.com/scl/fi/7ivrov8lckgh0962lvv69/OSM-tag-report.xlsx?dl=0&rlkey=s3yy1q8njyqrn0wmhaoue4c8m)
- gist: [gist.github.com/anisotropi4/757430340067ebfdd144bdd3ce92823c](https://gist.github.com/anisotropi4/757430340067ebfdd144bdd3ce92823c)
- bl.ocks [bl.ocks.org/anisotropi4/757430340067ebfdd144bdd3ce92823c](http://bl.ocks.org/anisotropi4/757430340067ebfdd144bdd3ce92823c)

## Prototype Maps
- Will's map: [map](http://bl.ocks.org/anisotropi4/raw/757430340067ebfdd144bdd3ce92823c/)
- Martin's map: [map](https://www.railwayhistory.org/electric/#?map=11!51.3965!-0.2703)
